package LogParser

import (
	"os"
	"fmt"
	"path/filepath"
	"strings"
	"regexp"
	"log"
	"bufio"
	"sort"

	"common"
)

type LogParser struct {
	Root string
	Regex *regexp.Regexp
	Voting map[string]uint32
}

func NewLogParser(path string) (*LogParser, error) {
	if len(path) == 0 {
		err := fmt.Errorf("%s has error, empty path.", common.GetFunctionName(NewLogParser))
		return nil, err
	}

	f, err := os.Open(path)
	if err != nil {
		err = fmt.Errorf("%s has error, %s", common.GetFunctionName(NewLogParser), err.Error())
		return nil, err
	}
	f.Close()

	re := regexp.MustCompile("controller=\\w+&action=\\w+")

	return &LogParser{
		Root: path,
		Regex: re,
		Voting: make(map[string]uint32),
	}, nil
}

func (lp *LogParser) Parse() {
	filepath.Walk(lp.Root, lp.WalkFunc)
}

func (lp *LogParser) parse(path string) {
	if strings.Index(path, "access") == -1 {
		return
	}

	f, err := os.Open(path)
	if err != nil {
		log.Printf("%s has error, %s", common.GetFunctionName(lp.parse))
		return
	}

	reader := bufio.NewReader(f)
	for {
		l, _, err := reader.ReadLine()
		if err != nil {
			break;
		}
		key := lp.Regex.FindString(string(l))
		lp.Voting[key]++
	}
}

func (lp *LogParser) WalkFunc(path string, info os.FileInfo, err error) error {
	if !info.IsDir() {
		lp.parse(path)
	}
	return nil
}

func (lp *LogParser) Save() {
	slice := make([]Function, 0)
	for k, v := range lp.Voting {
		slice = append(slice, Function {
			Name: k,
			Times: v,
		})
	}

	sort.Sort(ByTimes(slice))
	for _, v := range slice {
		fmt.Printf("%s: %d\n", v.Name, v.Times)
	}
}

type Function struct {
	Name string
	Times uint32
}

type ByTimes []Function

func (t ByTimes) Len() int {
	return len(t)
}

func (t ByTimes) Swap(i, j int) {
	t[i], t[j] = t[j], t[i]
}

func (t ByTimes) Less(i, j int) bool {
	return t[i].Times > t[j].Times
}
