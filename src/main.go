package main

import (
	"LogParser"
	"common"
)

func main() {
	common.InitLog("logParser.log")

	lp, err := LogParser.NewLogParser("../data/1110/")
	if err != nil {
		return
	}
	lp.Parse()
	lp.Save()
}
