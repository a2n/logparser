package common

import (
    "log"
    "runtime"
    "reflect"
    "os"
    "fmt"
)

func GetFunctionName(i interface{}) string {
    return runtime.FuncForPC(reflect.ValueOf(i).Pointer()).Name()
}

func InitLog(path string) error {
    if len(path) == 0 {
	err := fmt.Errorf("%s has error, empty path.", GetFunctionName(InitLog))
	return err
    }

    f, err := os.OpenFile(path, os.O_RDWR | os.O_APPEND | os.O_CREATE, 0666)
    if err != nil {
	err := fmt.Errorf("%s has error, %s.", GetFunctionName(InitLog), err.Error())
	return err
    }

    log.SetOutput(f)
    return nil
}
